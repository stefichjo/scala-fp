package inheritance

// (Scala 2)
object ImplicitStudy:

  case class Person(firstName: String, lastName: String)

  val jane = Person("Jane", "Doe")
  val john = Person("John", "Doe")

  val myOrderingInstance = new Ordering[Person]:
    def compare(a: Person, b: Person): Int =
      a.firstName compareTo b.firstName
  
  implicit val myImplicitOrderingInstance: Ordering[Person] = new Ordering[Person]:
    def compare(a: Person, b: Person): Int =
      a.firstName compareTo b.firstName

  def myCompare(a: Person, b: Person)(implicit orderingInstance: Ordering[Person]): Int =
    orderingInstance.compare(a, b)