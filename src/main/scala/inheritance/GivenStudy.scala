package inheritance

// (Scala 3)
object GivenStudy:

  case class Person(firstName: String, lastName: String)

  val jane = Person("Jane", "Doe")
  val john = Person("John", "Doe")

  val myOrderingInstance = new Ordering[Person]:
    def compare(a: Person, b: Person): Int =
      a.firstName compareTo b.lastName

  given Ordering[Person] with
    def compare(a: Person, b: Person): Int =
      a.firstName compareTo b.lastName

  def myCompare(a: Person, b: Person)(using orderingInstance: Ordering[Person]): Int =
    orderingInstance.compare(a, b)

