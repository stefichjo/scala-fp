package inheritance

object LambdaCaseStudy:

  def elvisHelper(default: Any): Option[Any] => Any =
    case None => default
    case Some(x) => x

  extension (option: Option[Any])
    def ?:(default: Any) = elvisHelper(default)(option)
