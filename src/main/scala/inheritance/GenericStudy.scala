package inheritance

object GenericStudy:

  def lengthAny: List[Any] => Int =
    case Nil => 0
    case _ :: tail => 1 + lengthAny(tail)

  def lengthTs[T]: List[T] => Int =
    case Nil => 0
    case _ :: tail => 1 + lengthTs(tail)
