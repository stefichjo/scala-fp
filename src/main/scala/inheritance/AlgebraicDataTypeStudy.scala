package inheritance

object AlgebraicDataTypeStudy:

  trait Shape:
    def circumference: Double

  case class Rectangle(width: Double, height: Double) extends Shape:
    override def circumference = width * height

  case class Circle(radius: Float) extends Shape:
    override def circumference = radius * Math.PI

  enum ShapeEnum:
    case RectangleCase(width: Double, height: Double)
    case CircleCase(radius: Double)

    def circumference = this match
      case RectangleCase(w, h) => w * h
      case CircleCase(r) => r * Math.PI
