package inheritance

object InstanceStudy:

  trait Animal:
    def makeSound: String

  class Cat extends Animal:
    def makeSound = "Meow."

  val cat = new Cat

  val cow = new Animal:
    def makeSound = "MOO!"
