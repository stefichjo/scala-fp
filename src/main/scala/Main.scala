object Main:

  def main(args: Array[String]): Unit =
    println("Hello World!")
    greet("Carl")

  // cf. unit tests in Welcome.scala
  def greeting(name: String): String = s"Hello $name!"

  // use greeting
  def greet(name: String): Unit = println(greeting(name))
