package values

object LazyListStudy:

  def sieve(prime: Int, numbers: LazyList[Int]): LazyList[Int] =
    numbers.filter(_ % prime != 0)

  def primes: LazyList[Int] =
    filterPrimes(LazyList.from(2))

  def filterPrimes: LazyList[Int] => LazyList[Int] =
     case prime #:: numbers => prime #:: filterPrimes(sieve(prime, numbers))
