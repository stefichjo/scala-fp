package values

object ListStudy:
  // use _.isEmpty, _.tail
  def countElementsHeadTail(xs: List[Any]): Int =
    if xs.isEmpty
    then 0
    else 1 + countElementsHeadTail(xs.tail)

  // use match/case, Nil, ::
  def countElementsConsNil(xs: List[Any]): Int =
    xs match
      case Nil => 0
      case _ :: tail => 1 + countElementsConsNil(tail)
