package values

object RecursionStudy:

  def factorialRec(n: Int): BigInt = n match
    case 0 => 1
    case _ => n * factorialRec(n - 1)

  @scala.annotation.tailrec
  def factorialTailrec(acc: BigInt = 1)(n: Int): BigInt = n match
    case 0 => acc
    case _ => factorialTailrec(n * acc)(n - 1)

  /*
    reduce
      1 :: 2 :: 3 :: Nil
      1 *  2 *  3
   */

  def factorialReduce(n: Int): BigInt = n match
    case 0 => 1
    case _ => (1 to n).map(BigInt(_)).reduce(_ * _)

  /*
    fold
      1 :: 2 :: 3 :: Nil
      1 *  2 *  3 *  1
   */

  def factorialFold(n: Int): BigInt =
    (1 to n).map(BigInt(_)).fold(1: BigInt)(_ * _)

