package values

object LazyStudy:

  val helloVal: Int =
    println("Hello val!") // evaluated when declared (only once)
    1 // memoized

  def helloDef: Int =
    println("Hello def!") // evaluated when needed
    2 // not memoized

  lazy val helloLazyVal: Int =
    println("Hello lazy val!") // evaluated when needed (only once)
    3 // memoized

  def hello: Unit =
    println("calling helloVal")
    println(helloVal)
    println(helloVal)

    println("calling helloDef")
    println(helloDef)
    println(helloDef)

    println("calling helloLazyVal")
    println(helloLazyVal)
    println(helloLazyVal)

