package values

object StringStudy:

  type Word = String
  type Sentence = String

  extension (sentence: Sentence)
    def words: List[Word] = sentence.split(" ").toList

  extension (words: List[Word])
    def unwords: Sentence = words.mkString(" ")

  def makeCoding(coding: Word => Word): Sentence => Sentence = _
    .words
    .map(coding)
    .unwords

  def coding(offset: Int): Word => Word = word =>
    for
      c <- word
    yield
      (c + offset).toChar

  def caesarEncode = makeCoding(coding(1))
  def caesarDecode = makeCoding(coding(-1))
