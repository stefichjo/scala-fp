package events
package adapters

import ports.*
import Language.*

object Interpretation:
  import cats.effect.IO
  import cats.data.State
  import cats.syntax.functor.*
  import io.circe.{ Decoder, Encoder }, io.circe.generic.auto.*, io.circe.parser.*, io.circe.syntax.*

  given Logs[IO] with
    def write = bankState => IO {
      println(bankState.toView.toJson)
      bankState
    }

  implicit val decodeOperation: Decoder[Command] =
    List[Decoder[Command]](
      Decoder[Command.CreateAccount].widen,
      Decoder[Command.MakeTransaction].widen
    )reduceLeft(_ or _)

  extension (it: String)
    def fromJson: Command = decode[Command](it) match
      case Left(parsingError) => throw new IllegalArgumentException(s"Invalid JSON object: ${parsingError}")
      case Right(command) => command

  extension (it: View)
    def toJson: String = it.asJson.noSpaces

