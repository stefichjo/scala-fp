package events
package adapters

import Interpretation.*, Interpretation.given
import ports.*
import Program.*

object Presentation:
  import scala.io.Source.fromFile
  import cats.effect.unsafe.implicits.global // unsafeRunSync

  val filename =
    // "operations-account-already-initialized"
    "operations-insufficient-limit"

  def run: Unit =
    fromFile(filename) // FIXME read from stdin
      .getLines
      .map(_.fromJson)
      .to(LazyList) // FIXME laziness is not preserved :(
      .authorize
      .unsafeRunSync()
