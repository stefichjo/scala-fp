package events
package adapters.ports

import Language.*
import core.*
import Domain.*

object Program:
  import cats.Monad
  import cats.syntax.all.* // flatMap

  extension (it: LazyList[Command])
    def authorize[F[_]: Logs: Monad]: F[Unit] =
      it.foldLeft(BankState.zero.pure)(authorizeF).map(_ => ())
