package events
package adapters.ports.core

object Domain:
  import java.time.LocalDateTime

  type Time = LocalDateTime

  type Amount = Int

  enum Event:
    case AccountCreated(isActive: Boolean, availableLimit: Amount)
    case TransactionCompleted(merchant: String, amount: Amount, time: Time)

    def isRecent(now: Time): Boolean =
      this match
        case transactionCompleted: Event.TransactionCompleted =>
          transactionCompleted.time.isAfter(now.minusMinutes(2))

        case _ => false

  case class Account(isActive: Boolean, availableLimit: Amount) //  TODO "limit" -> "Kontostand"
  object Account:
    val zero: Account = Account(false, 0)

  extension (it: Account)
    def update: Event => Account =
      case accountCreated: Event.AccountCreated =>
        Account(accountCreated.isActive, accountCreated.availableLimit)

      case transactionCompleted: Event.TransactionCompleted =>
        Account(it.isActive, it.availableLimit - transactionCompleted.amount)

  extension (it: List[Event])
    def toAccount: Account = it.foldLeft(Account.zero)(_.update(_))
    def recentUntil(now: Time): List[Event] = it.filter(_.isRecent(now))

  enum Violation:
    case AccountAlreadyInitialized
    case AccountNotInitialized
    case AccountNotActive
    case InsufficientLimit
    case HighFrequencySmallInterval
    case DoubledTransaction

  case class BankState(events: List[Event], violations: List[Violation])
  object BankState:
    def zero = BankState(Nil, Nil)

  