package events
package adapters.ports

import core.*
import Domain.*

object Language:
  import cats.Monad
  import cats.syntax.all.* // flatMap

  trait Logs[F[_]: Monad]:
    def write: BankState => F[BankState]
  
  type View = (Account, List[Violation])
  extension (it: BankState)
    def toView: View = (it.events.toAccount, it.violations)

  enum Command:
    case CreateAccount(isActive: Boolean, availableLimit: Amount)
    case MakeTransaction(merchant: String, amount: Amount, time: Time)

    def toEvent: Event =
      this match
        case Command.CreateAccount(isActive, availableLimit) =>
          Event.AccountCreated(isActive, availableLimit)
        case Command.MakeTransaction(merchant, amount, time) =>
          Event.TransactionCompleted(merchant, amount, time)

    def isDouble(event: Event): Boolean =
      this match
        case makeTransaction: Command.MakeTransaction => event match
          case transactionCompleted: Event.TransactionCompleted =>
            makeTransaction.merchant == transactionCompleted.merchant && makeTransaction.amount == transactionCompleted.amount

          case _ => false

        case _ => false

  extension (it: List[Event])
    def check(command: Command)(violation: Violation): Option[Violation] =

      command match
        case createAccount: Command.CreateAccount => violation match
          case Violation.AccountAlreadyInitialized
          if !it.isEmpty =>
            Some(violation)

          case _ => None

        case makeTransaction: Command.MakeTransaction => violation match
          case Violation.AccountNotInitialized
          if it.isEmpty =>
            Some(violation)

          case Violation.AccountNotActive
          if !it.isEmpty && !it.toAccount.isActive =>
            Some(violation)
          
          case Violation.InsufficientLimit
          if !it.isEmpty && it.toAccount.availableLimit < makeTransaction.amount =>
            Some(violation)

          case Violation.HighFrequencySmallInterval
          if it.recentUntil(makeTransaction.time).size >= 3 =>
            Some(violation)

          case Violation.DoubledTransaction
          if !it.recentUntil(makeTransaction.time).filter(makeTransaction.isDouble).isEmpty =>
            Some(violation)

          case _ => None

    def checkForViolations(command: Command): List[Violation] =
      Violation
        .values
        .toList
        .map(it.check(command))
        .flatten

  extension (it: BankState)
    def +(delta: BankState): BankState =
      BankState(
        it.events ++ delta.events,
        it.violations ++ delta.violations
      )

  extension (it: BankState)
    def execute(command: Command): BankState =
      val violationsDelta = it.events.checkForViolations(command)
      val eventsDelta = violationsDelta match
        case Nil => List(command.toEvent)
        case _ => Nil

      it + BankState(
        eventsDelta,
        violationsDelta
      )

  def authorizeF[F[_]: Logs: Monad](bankStateF: F[BankState], command: Command): F[BankState] =
      bankStateF.flatMap(
        bankState =>
          summon[Logs[F]].write(bankState.execute(command))
      )
