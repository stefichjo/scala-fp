package dependencies

object ComprehensionStudy:

  def sumOptions(x: Option[Int], y: Option[Int]) =
    if x.isEmpty then
      None
    else
      if y.isEmpty then
        None
      else Some(x.get + y.get)

  def sumOptionsFor(x: Option[Int], y: Option[Int]) =
    for
      a <- x
      b <- y
    yield
      a + b

  def around(n: Int): List[Int] = List(n - 1, n, n + 1)

  val ranks = (2 to 10).map(_.toString) ++ List("J", "Q", "K", "A")
  val suits = List("♦", "♥", "♠", "♣")

  def deck =
    for
      rank <- ranks
      suit <- suits
    yield
      rank ++ suit
