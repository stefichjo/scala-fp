package dependencies

import java.time.LocalDateTime
import java.io.File
import java.nio.file.{Files, StandardOpenOption}
import scala.io.Source.*

object RepositoryStudy:

  def main(args: Array[String]): Unit = Presentation.run

  // C: Core Logic
  object Domain:
    type Timestamp = LocalDateTime

  // P2: Secondary Ports
  object Language:
    import Domain.*

    trait Logs:
      def write(s: String): Unit

    trait Timestamps:
      def next: Timestamp
      def get: List[Timestamp]
      def put(a: Timestamp): Unit

    def timestampCount(using repository: Timestamps) =
      repository.get.length

  // A2: Secondary Adapters
  object Interpretation:
    import Language.*

    given Logs with
      def write(s: String) = println(s)

    given Timestamps with
      val filename = "timestamps"
      def next = LocalDateTime.now
      def get =
        fromFile(filename)
          .getLines
          .map(LocalDateTime.parse)
          .toList
      def put(timestamp: Domain.Timestamp) =
        Files.write(
          new File(filename).toPath,
          s"${timestamp.toString}\n".getBytes("UTF-8"),
          StandardOpenOption.APPEND)

  // P1: Primary Ports
  object Program:
    import Language.*

    def addNewTimestamp(using timestamps: Timestamps, logs: Logs) =

      val timestamp = timestamps.next
      logs.write(s"Next timestamp (#${timestampCount + 1}): $timestamp.")
      timestamps.put(timestamp)

  // A1: Primary Adapters
  object Presentation:
    import Interpretation.given
    import Program.*

    def run = addNewTimestamp
