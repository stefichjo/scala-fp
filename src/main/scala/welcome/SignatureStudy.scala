package welcome

object SignatureStudy:

  def subMethod1(n: Int, m: Int): Int = m - n

  def subMethod2(n: Int)(m: Int): Int = m - n

  def subOne(m: Int): Int = subMethod2(1)(m)
  
  def subOneLambda: Int => Int = m => subMethod2(1)(m)
  
  def subOneEtaReduced = subMethod2(1)
  
  extension (n: Int)
    def dec = subOneEtaReduced(n)

  extension (c: Char)
    def pos =
      if c.isLetter
      then Some(c.toLower.hashCode - 96)
      else None
