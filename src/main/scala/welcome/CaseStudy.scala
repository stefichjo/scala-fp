package welcome

object CaseStudy:

  def countDownFor(n: Int): Unit =
    for (x <- n to 1 by -1)
      print(s"$x! ")

    println("Lift off!")

  def countDownIfThenElse(n: Int): Unit =
    if n == 0
    then
      println("Lift off!")
    else
      print(s"$n! ")
      countDownIfThenElse(n - 1)

  def countDownCase(n: Int): Unit =
    n match
      case 0 =>
        println("Lift off!")
      case _ =>
        print(s"$n! ")
        countDownCase(n - 1)

  countDownFor(3)
  countDownIfThenElse(3)
  countDownCase(3)


