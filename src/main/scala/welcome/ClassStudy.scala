package welcome

object ClassStudy:

  class JavaStudent private() {
    var id: Int | Null = null
    var name: String = null

    def this(id: Int, name: String) = {
      this()
      this.id = id
      this.name = name
    }

    override def toString: String = {
      return s"JavaStudent($id,$name)"
    }

    override def hashCode: Int = {
      return id.hashCode * name.hashCode
    }

    override def equals(javaStudent: Any) = {
      return hashCode == javaStudent.hashCode
    }

  }

  case class ScalaStudent(var id: Int, var name: String)
