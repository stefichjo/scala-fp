package welcome

object IterationStudy:

  // var, for, return
  def sum(ns: List[Int]): Int =
    var acc = 0

    for (n <- ns)
      acc += n

    return acc

  // var, for, return
  def prod(ns: List[Int]): BigInt =
    var acc = 1

    for (n <- ns)
      acc *= n

    return acc

