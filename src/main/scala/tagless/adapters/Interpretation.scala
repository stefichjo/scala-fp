package tagless
package adapters

import ports.*
import Language.*

object Interpretation:
  import cats.effect.IO
  import java.time.LocalDateTime.{now, parse}
  import java.io.File
  import java.nio.file.{Files, StandardOpenOption}
  import scala.io.Source.fromFile

  given Logs[IO] with
    def write = s => IO { println(s) }

  given Timestamps[IO] with
    val filename = "timestamps"
    def next = IO { now }
    def get = IO {
      fromFile(filename)
        .getLines
        .map(parse)
        .toList
    }
    def put = timestamp => IO {
      Files.write(
        new File(filename).toPath,
        s"${timestamp.toString}\n".getBytes("UTF-8"),
        StandardOpenOption.APPEND)
      ()
    }

