package tagless
package adapters

import Interpretation.given
import ports.*
import Program.*

object Presentation:
  import cats.effect.unsafe.implicits.global // unsafeRunSync

  def run: Unit = addNewTimestamp.unsafeRunSync()
  