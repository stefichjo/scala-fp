package tagless
package adapters.ports

import Language.*

object Program:
  import cats.Monad
  import cats.syntax.all.* // flatMap

  def addNewTimestamp[F[_]: Logs: Timestamps: Monad]: F[Unit] =
    val logs = summon[Logs[F]]
    val timestamps = summon[Timestamps[F]]
    import logs.*, timestamps.*

    for
      timestamp <- next
      count <- timestampCount
      _ <- write(s"Next timestamp (#${count + 1}): $timestamp.")
      _ <- put(timestamp)
    yield
      ()

