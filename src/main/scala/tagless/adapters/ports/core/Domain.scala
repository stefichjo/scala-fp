package tagless
package adapters.ports.core

object Domain:
  import java.time.LocalDateTime

  type Timestamp = LocalDateTime

