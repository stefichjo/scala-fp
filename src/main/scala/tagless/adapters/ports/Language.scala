package tagless
package adapters.ports

import core.*
import Domain.*

object Language:
  import cats.Monad
  import cats.syntax.all.* // flatMap

  trait Logs[F[_]: Monad]:
    def write: String => F[Unit]
  
  trait Timestamps[F[_]: Monad]:
    def next: F[Timestamp]
    def get: F[List[Timestamp]]
    def put: Timestamp => F[Unit]

  def timestampCount[F[_]: Timestamps: Monad] =
    val timestamps = summon[Timestamps[F]]
    import timestamps.*

    for
      all <- get
    yield
      all.length

