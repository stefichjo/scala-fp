import org.junit.Test

class Inheritance:

  @Test
  def algebraicDataTypeTests =
    import inheritance.AlgebraicDataTypeStudy.*
    import inheritance.AlgebraicDataTypeStudy.ShapeEnum.*

    // trait
    assert(10 == Rectangle(5, 2).circumference)
    assert(10 == Circle(3.18).circumference.round)

    // enum
    assert(10 == RectangleCase(5, 2).circumference)
    assert(10 == CircleCase(3.18).circumference.round)

  @Test
  def lambdaCaseTest =
    import inheritance.LambdaCaseStudy.*

    // case lambda
    assert(42 == elvisHelper(23)(Some(42)))
    assert(23 == elvisHelper(23)(None))

    // extension method
    assert(42 == Some(42) ?: 23)
    assert(23 == None ?: 23)

  @Test
  def genericTests =
    import inheritance.GenericStudy.*

    // Any, T
    assert(2 == lengthAny(List(23, "Hello")))
    assert(2 == lengthTs(List(23, "Hello")))

  @Test
  def instanceTests =
    import inheritance.InstanceStudy.*

    assert("Meow." == cat.makeSound)
    assert("MOO!" == cow.makeSound)

  @Test
  def compareTests =
    assert(14 == "John".compareTo("Jane"))

  @Test
  def givenTests =
    import inheritance.GivenStudy.*

    assert(6 == myOrderingInstance.compare(john, jane))
    assert(6 == myCompare(john, jane)(using myOrderingInstance))

    assert(6 == summon[Ordering[Person]].compare(john, jane))
    assert(6 == myCompare(john, jane)(using summon[Ordering[Person]]))
    assert(6 == myCompare(john, jane))

  @Test
  def implicitTests =
    import inheritance.ImplicitStudy.*

    assert(14 == myOrderingInstance.compare(john, jane))
    assert(14 == myCompare(john, jane)(myOrderingInstance))

    assert(14 == implicitly[Ordering[Person]].compare(john, jane))
    assert(14 == myCompare(john, jane)(implicitly[Ordering[Person]]))
    assert(14 == myCompare(john, jane))
