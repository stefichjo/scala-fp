import org.junit.Test

import tagless.*

class Tagless:
  import cats.Monad
  import cats.syntax.all.* // flatMap
  import cats.effect.IO
  import cats.effect.unsafe.implicits.global // unsafeRunSync
  import cats.data.State
  import java.time.LocalDateTime.now

  @Test
  def taglessFinalIoTests =
    import adapters.ports.Language.*
    import adapters.ports.Program.*
    import adapters.ports.core.Domain.*

    given Logs[IO] with
      def write = s => IO { () }

    var timestamps: IO[List[Timestamp]] = IO { Nil }
    given Timestamps[IO] with
      def next = IO { now }
      def get = timestamps
      def put = timestamp =>
        timestamps = timestamps.map(timestamp :: _)
        IO { () }

    timestamps = IO { Nil }
    assert(0 == timestampCount.unsafeRunSync())

    timestamps = IO { Nil }
    addNewTimestamp.unsafeRunSync()
    assert(1 == timestampCount.unsafeRunSync())

    timestamps = IO { Nil }
    addNewTimestamp.unsafeRunSync()
    addNewTimestamp.unsafeRunSync()
    assert(2 == timestampCount.unsafeRunSync())

  @Test
  def taglessFinalStateTests =
    import adapters.ports.Language.*
    import adapters.ports.Program.*
    import adapters.ports.core.Domain.*

    type MyStateful[A] = State[List[Timestamp], A]

    given Logs[MyStateful] with
      def write = _ => State((_, ()))

    given Timestamps[MyStateful] with
      def next = State((_, now))
      def get = State(timestamps => (timestamps, timestamps))
      def put = timestamp => State(timestamps => (timestamp :: timestamps, ()))

    assert(0 == timestampCount.runA(Nil).value)
    assert(1 == timestampCount.runA(List(now)).value)
    assert(() == addNewTimestamp.runA(Nil).value)

    def myProgram[F[_]: Logs: Timestamps: Monad]: F[Int] =
      for
        _ <- addNewTimestamp
        _ <- addNewTimestamp
        count <- timestampCount
      yield
        count

    assert(2 == myProgram.runA(Nil).value)
    assert(3 == myProgram.runA(List(now)).value)
