import org.junit.Test

import cats.implicits.*

class Patterns:

  @Test
  def monoidTests =
    // https://www.scala-exercises.org/cats/semigroup
    // https://www.scala-exercises.org/cats/monoid

    import cats.Monoid

    assert(43 == (42 |+| 1))
    assert(42 == (42 |+| 0))
    assert(0 == Monoid[Int].empty)
    assert(42 == (42 |+| Monoid[Int].empty))

    assert("Hi!" == ("Hi" |+| "!"))
    assert("Hi" == ("Hi" |+| ""))
    assert("" == Monoid[String].empty)
    assert("Hi" == ("Hi" |+| Monoid[String].empty))

    assert(Option(43) == (Option(42) |+| Option(1)))
    assert(Option(42) == (Option(42) |+| None))
    assert(None == Monoid[Option[Int]].empty)
    assert(Option(42) == (Option(42) |+| Monoid[Option[Int]].empty))

    assert(List(1, 2, 3) == (List(1, 2) |+| List(3)))
    assert(List(1, 2) == (List(1, 2) |+| Nil))
    assert(Nil == Monoid[List[Int]].empty)
    assert(List(1, 2) == (List(1, 2) |+| Monoid[List[Int]].empty))

  @Test
  def functorTests =
    // https://www.scala-exercises.org/cats/functor

    import cats.Functor

    assert(Option(5) == Functor[Option].map(Option("Hello"))(_.length))
    assert(Option(5) == Option("Hello").map(_.length))

    assert(List(4, 5) == Functor[List].map(List("qwer", "asdfg"))(_.length))
    assert(List(4, 5) == List("qwer", "asdfg").map(_.length))

    assert(
      List(Option(4), None, Option(5)) ==
        (Functor[List] compose Functor[Option])
          .map(List(Option("qwer"), None, Option("asdfg")))(_.length)
    )

    assert(List(List(42, 42), List(23, 23)) == Functor[List].map(List(42, 23))(x => List(x, x)))

  @Test
  def applicativeTests =
    // https://www.scala-exercises.org/cats/apply
    // https://www.scala-exercises.org/cats/applicative

    import cats.Applicative

    assert(Option(5) == Applicative[Option].pure(5))
    assert(List(5) == Applicative[List].pure(5))
    assert(List(Option(5)) == (Applicative[List] compose Applicative[Option]).pure(5))

    assert(Option(5) == Applicative[Option].ap(Option((_: String).length))(Option("Hello")))
    assert(List(4, 5) == Applicative[List].ap(List((_: String).length))(List("qwer", "asdfg")))
    assert(List(41, 43) == Applicative[List].ap(List((_: Int) - 1, (_: Int) + 1))(List(42)))
    assert(List(41, 22, 43, 24) == Applicative[List].ap(List((_: Int) - 1, (_: Int) + 1))(List(42, 23)))

  @Test
  def monadTests =
    // https://www.scala-exercises.org/cats/monad

    import cats.Monad

    assert(Option(42) == Option(Option(42)).flatten)
    assert(None == Option(None).flatten)
    assert(List(42, 42, 23, 23) == List(List(42, 42), List(23, 23)).flatten)
    assert(Nil == List(Nil).flatten)

    assert(
      List(List(42, 42), List(23, 23)) ==
        Monad[List].map(List(42, 23))(x => List(x, x)))
    assert(
      List(42, 42, 23, 23) ==
        Monad[List].map(List(42, 23))(x => List(x, x)).flatten)
    assert(
      List(42, 42, 23, 23) ==
        Monad[List].flatMap(List(42, 23))(x => List(x, x)))
