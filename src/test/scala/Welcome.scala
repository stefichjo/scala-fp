import org.junit.Test

class Welcome:

  @Test
  def mainTests =
    import Main.*

    assert("Hello Carl!" == greeting("Carl"))

  @Test
  def caseTests =
    import welcome.CaseStudy.*

    def isMeaningful(xs: List[Int]): Boolean =
      for (42 <- xs)
        return true
      
      return false

    assert(!isMeaningful(List()))
    assert(!isMeaningful(List(23)))
    assert(isMeaningful(List(42)))
    assert(isMeaningful(List(23, 42)))
    assert(isMeaningful(List(42, 23)))

  @Test
  def classTests =
    import welcome.ClassStudy.*

    // class
    var javaStudent = new JavaStudent(42, "Carl")
    assert(javaStudent == new JavaStudent(42, "Carl"))
    assert(javaStudent != new JavaStudent(42, "Caaarl!!!"))
    assert("JavaStudent(42,Carl)" == javaStudent.toString)
    javaStudent.name = "Caaarl!!!"
    assert("Caaarl!!!" == javaStudent.name)

    // case class
    var scalaStudent = ScalaStudent(42, "Carl")
    assert(scalaStudent == ScalaStudent(42, "Carl"))
    assert(scalaStudent != ScalaStudent(42, "Caaarl!!!"))
    assert("ScalaStudent(42,Carl)" == scalaStudent.toString)
    scalaStudent.name = "Caaarl!!!"
    assert("Caaarl!!!" == scalaStudent.name)

  @Test
  def iterationTests =
    import welcome.IterationStudy.*

    assert(10 == sum((1 to 4).toList))
    assert(0 == sum(List()))
    assert(24 == prod((1 to 4).toList))
    assert(1 == prod(List()))

  @Test
  def signatureTests =
    import welcome.SignatureStudy.*

    // 1-argument class method in infix operator syntax
    assert("Hello".contains("ell"))
    assert("Hello" contains "ell")
    assert(41 == 42 - 1)
    assert(41 == 42.-(1))

    // partial function application
    assert(41 == subMethod1(1, 42))
    assert(41 == subMethod2(1)(42))
    assert(41 == subOne(42))
    assert(41 == subOneLambda(42))
    assert(41 == subOneEtaReduced(42))
    assert(41 == 42.dec)

    // extension method
    assert(Some(1) == 'a'.pos)
    assert(None == '@'.pos)
