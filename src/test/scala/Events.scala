import org.junit.Test

import events.*

class Events:

  object Fixtures:
    import adapters.ports.core.*
    import Domain.*

    val myTimes: List[Time] =
      List(
        "2021-08-19T10:00:00.123456",
        "2021-08-19T10:00:30.123456",
        "2021-08-19T10:01:00.123456",
        "2021-08-19T10:01:30.123456",
        "2021-08-19T10:02:00.123456",
        "2021-08-19T10:02:30.123456",
        "2021-08-19T10:03:00.123456"
      ).map(java.time.LocalDateTime.parse)

    val myAccountCreated = Event.AccountCreated(true, 100)
    val myAccountCreatedInactive = Event.AccountCreated(false, 100)
    val myTransactionCompleted = Event.TransactionCompleted("Burger King", 20, myTimes(0))

    val myEvents = myAccountCreated :: myTimes.map(Event.TransactionCompleted("Foo", 42, _))

  @Test
  def domainTests =
    import adapters.ports.core.Domain.*
    import Fixtures.*

    assert(Account(false, 0) == Nil.toAccount)
    assert(Account(true, 100) == List(myAccountCreated).toAccount)
    assert(Account(false, 100) == List(myAccountCreatedInactive).toAccount)
    assert(Account(true, 80) == List(myAccountCreated, myTransactionCompleted).toAccount)

    val myTransactionEvents = myEvents.drop(1)

    assert(myTransactionEvents == myEvents.recentUntil(myTimes(0)))
    assert(myTransactionEvents == myEvents.recentUntil(myTimes(1)))
    assert(myTransactionEvents == myEvents.recentUntil(myTimes(2)))
    assert(myTransactionEvents == myEvents.recentUntil(myTimes(3)))
    assert(myTransactionEvents.drop(1) == myEvents.recentUntil(myTimes(4)))
    assert(myTransactionEvents.drop(2) == myEvents.recentUntil(myTimes(5)))
    assert(myTransactionEvents.drop(3) == myEvents.recentUntil(myTimes(6)))

  @Test
  def languageTests =
    import adapters.ports.Language.*
    import adapters.ports.core.Domain.*
    import Fixtures.*

    assert(
      Event.AccountCreated(true, 123)
      ==
      Command.CreateAccount(true, 123).toEvent
    )
    assert(
      Event.TransactionCompleted("abc", 123, myTimes(0))
      ==
      Command.MakeTransaction("abc", 123, myTimes(0)).toEvent
    )

    assert(
      Nil
      ==
      List(
        Event.AccountCreated(true, 123)
      ).checkForViolations(
        Command.MakeTransaction("abc", 123, myTimes(0))
      )
    )
    assert(
      List(Violation.AccountNotInitialized)
      ==
      List(
      ).checkForViolations(
        Command.MakeTransaction("abc", 123, myTimes(0))
      )
    )
    
    assert(
      Nil
      ==
      List(
      ).checkForViolations(
        Command.CreateAccount(true, 123)
      )
    )
    assert(
      List(Violation.AccountAlreadyInitialized)
      ==
      List(
        Event.AccountCreated(true, 123)
      ).checkForViolations(
        Command.CreateAccount(true, 123)
      )
    )

    assert(
      Nil
      ==
      List(
        Event.AccountCreated(true, 123)
      ).checkForViolations(
        Command.MakeTransaction("abc", 123, myTimes(0))
      )
    )
    assert(
      List(Violation.AccountNotActive)
      ==
      List(
        Event.AccountCreated(false, 123)
      ).checkForViolations(
        Command.MakeTransaction("abc", 123, myTimes(0))
      )
    )

    assert(
      Nil
      ==
      List(
        Event.AccountCreated(true, 123)
      ).checkForViolations(
        Command.MakeTransaction("abc", 123, myTimes(0))
      )
    )
    assert(
      List(Violation.InsufficientLimit)
      ==
      List(
        Event.AccountCreated(true, 123)
      ).checkForViolations(
        Command.MakeTransaction("abc", 1234, myTimes(0))
      )
    )

    assert(
      Nil
      ==
      List(
        Event.AccountCreated(true, 1234),
        Event.TransactionCompleted("abc", 123, myTimes(1)),
        Event.TransactionCompleted("def", 123, myTimes(2))
      ).checkForViolations(
        Command.MakeTransaction("ghi", 123, myTimes(3))
      )
    )
    assert(
      List(Violation.HighFrequencySmallInterval)
      ==
      List(
        Event.AccountCreated(true, 1234),
        Event.TransactionCompleted("abc", 123, myTimes(1)),
        Event.TransactionCompleted("def", 123, myTimes(2)),
        Event.TransactionCompleted("ghi", 123, myTimes(3))
      ).checkForViolations(
        Command.MakeTransaction("jkl", 123, myTimes(4))
      )
    )

    assert(
      Nil
      ==
      List(
        Event.AccountCreated(true, 1234)
      ).checkForViolations(
        Command.MakeTransaction("abc", 123, myTimes(0))
      )
    )
    assert(
      List(Violation.DoubledTransaction)
      ==
      List(
        Event.AccountCreated(true, 1234),
        Event.TransactionCompleted("abc", 123, myTimes(0))
      ).checkForViolations(
        Command.MakeTransaction("abc", 123, myTimes(1))
      )
    )

    assert(
      List(
        Violation.HighFrequencySmallInterval,
        Violation.DoubledTransaction
      )
      ==
      List(
        Event.AccountCreated(true, 1234),
        Event.TransactionCompleted("abc", 123, myTimes(1)),
        Event.TransactionCompleted("def", 123, myTimes(2)),
        Event.TransactionCompleted("ghi", 123, myTimes(3))
      ).checkForViolations(
        Command.MakeTransaction("def", 123, myTimes(4))
      )
    )

    assert(
      List(
        Violation.InsufficientLimit,
        Violation.HighFrequencySmallInterval,
        Violation.DoubledTransaction
      )
      ==
      List(
        Event.AccountCreated(true, 369),
        Event.TransactionCompleted("abc", 123, myTimes(1)),
        Event.TransactionCompleted("def", 123, myTimes(2)),
        Event.TransactionCompleted("ghi", 123, myTimes(3))
      ).checkForViolations(
        Command.MakeTransaction("def", 123, myTimes(4))
      )
    )

    assert(
      List(
        Violation.AccountNotActive,
        Violation.InsufficientLimit,
        Violation.HighFrequencySmallInterval,
        Violation.DoubledTransaction
      )
      ==
      List(
        Event.AccountCreated(false, 369),
        Event.TransactionCompleted("abc", 123, myTimes(1)),
        Event.TransactionCompleted("def", 123, myTimes(2)),
        Event.TransactionCompleted("ghi", 123, myTimes(3))
      ).checkForViolations(
        Command.MakeTransaction("def", 123, myTimes(4))
      )
    )

    assert(
      BankState.zero
      ==
      BankState.zero + BankState.zero
    )
    assert(
      BankState(List(Event.AccountCreated(true, 123)), List())
      ==
      BankState.zero
        + BankState(List(Event.AccountCreated(true, 123)), List())
    )
    assert(
      BankState(
        List(
          Event.AccountCreated(true, 123),
          Event.TransactionCompleted("abc", 123, myTimes(0))
        ),
        List()
      )
      ==
      BankState.zero
        + BankState(List(Event.AccountCreated(true, 123)), List())
        + BankState(List(Event.TransactionCompleted("abc", 123, myTimes(0))), List())
    )
    assert(
      BankState(
        List(
          Event.AccountCreated(true, 123),
          Event.TransactionCompleted("abc", 123, myTimes(0))
        ),
        List(
          Violation.InsufficientLimit
        )
      )
      ==
      BankState.zero
        + BankState(List(Event.AccountCreated(true, 123)), List())
        + BankState(List(Event.TransactionCompleted("abc", 123, myTimes(0))), List())
        + BankState(List(), List(Violation.InsufficientLimit))
    )

    assert(
      BankState(
        List(
          Event.AccountCreated(true, 123),
          Command.MakeTransaction("abc", 123, myTimes(0)).toEvent
        ),
        Nil
      )
      ==
      BankState(
        List(Event.AccountCreated(true, 123)),
        Nil
      ).execute(
        Command.MakeTransaction("abc", 123, myTimes(0))
      )
    )
    assert(
      BankState(
        Nil,
        List(
          Violation.AccountNotInitialized
        )
      )
      ==
      BankState.zero.execute(
        Command.MakeTransaction("abc", 123, myTimes(0))
      )
    )
    assert(
      BankState(
        Nil,
        List(
          Violation.AccountNotInitialized,
          Violation.AccountNotInitialized
        )
      )
      ==
      BankState(
        Nil,
        List(
          Violation.AccountNotInitialized
        )
      ).execute(
        Command.MakeTransaction("abc", 123, myTimes(0))
      )
    )
    assert(
      BankState(
        Nil,
        List(
          Violation.AccountNotInitialized,
          Violation.AccountNotInitialized
        )
      )
      ==
      BankState.zero.execute(
        Command.MakeTransaction("abc", 123, myTimes(0))
      ).execute(
        Command.MakeTransaction("abc", 123, myTimes(0))
      )
    )

    assert(
      (List(myAccountCreated).toAccount, List(Violation.AccountAlreadyInitialized))
      ==
      BankState(List(myAccountCreated), List(Violation.AccountAlreadyInitialized)).toView
    )

  @Test
  def programTests =
    import adapters.ports.Program.*
    import adapters.ports.Language.*
    import adapters.ports.core.Domain.*
    import Fixtures.*

    import cats.data.State
    import cats.effect.unsafe.implicits.global
    
    type InMemoryViews[A] = State[List[BankState], A]
    given Logs[InMemoryViews] with
      def write = bankState => State(bankStates => (bankState :: bankStates, bankState))

    assert(
      List(
        BankState.zero
      )
      ==
      LazyList().authorize.runS(List(BankState.zero)).value
    )
    assert(
      List(
        BankState(
          List(),
          List(Violation.AccountNotInitialized)
        ),
        BankState.zero
      )
      ==
      LazyList(
        Command.MakeTransaction("abc", 123, myTimes(0))
      ).authorize.runS(List(BankState.zero)).value
    )
    assert(
      List(
        BankState(
          List(),
          List(Violation.AccountNotInitialized)
        ),
        BankState.zero
      )
      ==
      LazyList(
        Command.MakeTransaction("abc", 123, myTimes(0))
      ).authorize.runS(List(BankState.zero)).value
    )

  @Test
  def interpretationTests =
    import adapters.Interpretation.*
    import adapters.ports.Program.*
    import adapters.ports.Language.*
    import adapters.ports.core.Domain.*
    import Fixtures.*

    assert(
      """[{"isActive":true,"availableLimit":100},[{"AccountAlreadyInitialized":{}}]]"""
      ==
      BankState(List(myAccountCreated), List(Violation.AccountAlreadyInitialized)).toView.toJson
    )

    assert(
      Command.CreateAccount(true, 123)
      ==
      """{ "isActive": true, "availableLimit": 123 }""".fromJson
    )

    assert(
      Command.MakeTransaction("abc", 123, myTimes(0))
      ==
      """{ "merchant": "abc", "amount": 123, "time": "2021-08-19T10:00:00.123456" }""".fromJson
    )

    import cats.data.State
    import cats.effect.unsafe.implicits.global

    type InMemoryViews[A] = State[List[BankState], A]
    given Logs[InMemoryViews] with
      def write = bankState => State(bankStates => (bankStates ++ List(bankState), bankState))

    assert(
      """
        [{"isActive":true,"availableLimit":100},[]]
        [{"isActive":true,"availableLimit":80},[]]
        [{"isActive":true,"availableLimit":80},[{"InsufficientLimit":{}}]]    
      """
        .split("\n").toList.map(_.trim).filter(_ != "")
      ==
      """
        {"isActive":true,"availableLimit":100}
        {"merchant":"Hasir Burger","amount":20,"time":"2021-08-19T10:00:00.123456"}
        {"merchant":"Fletcher's","amount":90,"time":"2021-08-19T10:00:30.123456"}
      """
        .split("\n").toList.map(_.trim).filter(_ != "")
        .map(_.fromJson).to(LazyList).authorize.runS(List()).value.map(_.toView.toJson)
    )

    assert(
      """
        [{"isActive":true,"availableLimit":100},[]]
        [{"isActive":true,"availableLimit":100},[{"AccountAlreadyInitialized":{}}]]
      """
        .split("\n").toList.map(_.trim).filter(_ != "")
      ==
      """
        {"isActive":true,"availableLimit":100}
        {"isActive":true,"availableLimit":350}
      """
        .split("\n").toList.map(_.trim).filter(_ != "")
        .map(_.fromJson).to(LazyList).authorize.runS(List()).value.map(_.toView.toJson)
    )

class Json:

  @Test
  def jsonNestedTests = // https://www.baeldung.com/scala/circe-json
    import io.circe.*, io.circe.generic.auto.*, io.circe.parser.*, io.circe.syntax.*
    import io.circe.generic.semiauto.deriveDecoder

    case class Nested(arrayField: List[Int])
    val nested: Nested = Nested(List(1, 2, 3))
    val nestedJsonString: String = nested.asJson.noSpaces

    implicit val nestedDecoder: Decoder[Nested] = deriveDecoder[Nested]
    val nestedJsonStringParsed: Either[Error, Nested] = decode[Nested](nestedJsonString)

    assert(nested == nestedJsonStringParsed.getOrElse(null))

    case class Our(textField: String, numericField: Int, booleanField: Boolean, nestedObject: Nested)
    val our: Our = Our("textContent", 123, true, nested)
    val ourJsonString: String = our.asJson.noSpaces

    implicit val ourDecoder: Decoder[Our] = deriveDecoder[Our]
    val ourJsonStringParsed: Either[Error, Our] = decode[Our](ourJsonString)

    assert(our == ourJsonStringParsed.getOrElse(null))

  @Test
  def jsonAdtTests = // https://circe.github.io/circe/codecs/adt.html
    sealed trait Event

    case class Foo(i: Int) extends Event
    case class Bar(s: String) extends Event
    case class Baz(c: Char) extends Event
    case class Qux(values: List[String]) extends Event

    object GenericDerivation:
      import cats.syntax.functor.*
      import io.circe.{ Decoder, Encoder }, io.circe.generic.auto.*
      import io.circe.syntax.*

      implicit val encodeEvent: Encoder[Event] = Encoder.instance {
        case foo @ Foo(_) => foo.asJson
        case bar @ Bar(_) => bar.asJson
        case baz @ Baz(_) => baz.asJson
        case qux @ Qux(_) => qux.asJson
      }

      implicit val decodeEvent: Decoder[Event] =
        List[Decoder[Event]](
          Decoder[Foo].widen,
          Decoder[Bar].widen,
          Decoder[Baz].widen,
          Decoder[Qux].widen
        )reduceLeft(_ or _)

    import GenericDerivation.*
    import io.circe.parser.decode

    assert(
      Right(Foo(1000))
      ==
      decode[Event]("""{ "i": 1000 }""")
    )
    assert(
      Right(Bar("lambda"))
      ==
      decode[Event]("""{ "s": "lambda" }""")
    )
    assert(
      Right(Baz('λ'))
      ==
      decode[Event]("""{ "c": "λ" }""")
    )
    assert(
      Right(Qux(List("abc", "def", "ghi")))
      ==
      decode[Event]("""{ "values": ["abc", "def", "ghi"] }""")
    )

    // FIXME better JSON format
    // https://github.com/circe/circe-generic-extras
    // import io.circe.generic.extras.*
    // libraryDependencies += "io.circe" %%% "circe-generic-extras" % "0.14.1"
    // https://mvnrepository.com/artifact/io.circe/circe-generic-extras