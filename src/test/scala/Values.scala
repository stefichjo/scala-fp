import org.junit.Test

class Values:

  @Test
  def foreachTests =

    val forValue =
      var acc = 0
      for (n <- List(42, 23)) acc += n
      acc

    assert(65 == forValue)

    val foreachValue =
      var acc = 0
      List(42, 23).foreach(n => acc += n)
      acc

    assert(65 == foreachValue)

  @Test
  def mapTests =

    val forYieldValue =
      for n <- List(42, 23)
      yield n + 1

    assert(List(43, 24) == forYieldValue)

    val mapValue = List(42, 23).map(n => n + 1)

    assert(List(43, 24) == mapValue)

  @Test
  def filterTests =
    val forIf =
      for n <- List(1, 2, 3, 4, 5, 6) if n % 2 == 0
      yield n

    assert(List(2, 4, 6) == forIf)

    val filterValue =
      List(1, 2, 3, 4, 5, 6).filter(n => n % 2 == 0)

    assert(List(2, 4, 6) == filterValue)

  @Test
  def listTests =
    import values.ListStudy.*

    val list = List(42, 23)

    // take
    assert(List() == list.take(0))
    assert(List(42) == list.take(1))
    assert(List(42, 23) == list.take(2))
    assert(List(42, 23) == list.take(3))

    // cons, nil
    assert(list == 42 :: List(23))
    assert(list == 42 :: 23 :: List())
    assert(list == 42 :: 23 :: Nil)

    // head, tail
    assert(list == list.head :: list.tail)

    assert(2 == countElementsHeadTail(List(42, 23)))
    assert(2 == countElementsConsNil(List(42, 23)))

  @Test
  def stringTests =
    import values.StringStudy.*

    // sentence to words, words to sentence
    assert(List("Hello", "World!") == "Hello World!".words)
    assert("Hello World!" == List("Hello", "World!").unwords)

    // caesar cipher
    assert("Ifmmp Xpsme\"" == caesarEncode("Hello World!"))
    assert("Hello World!" == caesarDecode("Ifmmp Xpsme\""))

  @Test
  def recursionTests =
    import values.RecursionStudy.*

    val factorial100 = BigInt("93326215443944152681699238856266700490715968264381621468592963895217599993229915608941463976156518286253697920827223758251185210916864000000000000000000000000")
    assert(factorial100 == factorialRec(100))
    assert(factorial100 == factorialTailrec()(100))
    assert(factorial100 == factorialReduce(100))
    assert(factorial100 == factorialFold(100))

  @Test
  def lazyListTests =
    import values.LazyListStudy.*

    val lazyList = LazyList(23, 42)

    // prepend
    assert(lazyList == 23 #:: LazyList(42))
    assert(lazyList == 23 #:: 42 #:: LazyList())

    // take
    val naturalNumbers: LazyList[Int] = LazyList.from(1)
    assert((1 to 10).toList == naturalNumbers.take(10).toList)

    // primes
    assert(List(3, 5, 7, 9) == sieve(2, LazyList(3, 4, 5, 6, 7, 8, 9, 10)).toList)
    assert(List(5, 7) == sieve(3, LazyList(5, 7, 9)).toList)
    assert(7919 == primes(999))
  
  @Test
  def lazyTests =
    import values.LazyStudy.*

