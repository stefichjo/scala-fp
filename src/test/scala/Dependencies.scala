import org.junit.Test

import dependencies.*

class Dependencies:

  @Test
  def repositoryTests =
    import RepositoryStudy.*
    import Language.*
    import Program.*

    given Logs with
      def write(s: String) = ()

    given Timestamps with
      var timestamps: List[Domain.Timestamp] = Nil
      def next = java.time.LocalDateTime.now
      def get = timestamps
      def put(timestamp: Domain.Timestamp) = timestamps ::= timestamp

    assert(0 == timestampCount)

    addNewTimestamp
    assert(1 == timestampCount)

    addNewTimestamp
    assert(2 == timestampCount)

  @Test
  def someTests =
    import dependencies.ComprehensionStudy.*

    assert(None == sumOptions(None, None))
    assert(None == sumOptions(Some(42), None))
    assert(None == sumOptions(None, Some(23)))
    assert(Some(65) == sumOptions(Some(42), Some(23)))

    assert(None == sumOptionsFor(None, None))
    assert(None == sumOptionsFor(Some(42), None))
    assert(None == sumOptionsFor(None, Some(23)))
    assert(Some(65) == sumOptionsFor(Some(42), Some(23)))

  @Test
  def comprehensionTests =
    import dependencies.ComprehensionStudy.*

    val list = List(42, 23)

    assert(list == (
      for (n <- list)
      yield n
    ))

    val nestedList = List(List(41, 42, 43), List(22, 23, 24))
    assert(nestedList == list.map(around))
    assert(nestedList == (
      for (n <- list)
        yield around(n)
      ))

    val flattenedList = List(41, 42, 43, 22, 23, 24)
    assert(flattenedList == nestedList.flatten)
    assert(flattenedList == list.map(around).flatten)
    assert(flattenedList == list.flatMap(around))
    assert(flattenedList == (
      for
        n <- list
        aroundN <- around(n)
      yield
        aroundN
      ))

    assert(deck == List(
      "2♦", "2♥", "2♠", "2♣",
      "3♦", "3♥", "3♠", "3♣",
      "4♦", "4♥", "4♠", "4♣",
      "5♦", "5♥", "5♠", "5♣",
      "6♦", "6♥", "6♠", "6♣",
      "7♦", "7♥", "7♠", "7♣",
      "8♦", "8♥", "8♠", "8♣",
      "9♦", "9♥", "9♠", "9♣",
      "10♦", "10♥", "10♠", "10♣",
      "J♦", "J♥", "J♠", "J♣",
      "Q♦", "Q♥", "Q♠", "Q♣",
      "K♦", "K♥", "K♠", "K♣",
      "A♦", "A♥", "A♠", "A♣"))
