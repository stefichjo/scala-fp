## Scala 3

* Please download [Scala 3](https://www.scala-lang.org/download/)
* During the Study Group, I'll use [IntelliJ IDEA CE](https://www.jetbrains.com/de-de/idea/)
* Repository based on [scala3-example-project](https://github.com/scala/scala3-example-project/blob/main/README.md).
  * IntelliJ IDEA CE
    * File > New > Project > Scala, Dotty

### Sessions

#### Session 1: Welcome

* [Main](src/main/scala/Main.scala)
* [Case Study](src/main/scala/welcome/CaseStudy.scala)
* [Class Study](src/main/scala/welcome/ClassStudy.scala)
* [Iteration Study](src/main/scala/welcome/IterationStudy.scala)
* [Signature Study](src/main/scala/welcome/SignatureStudy.scala)

#### Session 2: Values

* [List Study](src/main/scala/values/ListStudy.scala)
* [String Study](src/main/scala/values/StringStudy.scala)
* [Recursion Study](src/main/scala/values/RecursionStudy.scala)
* [Lazy List Study](src/main/scala/values/LazyListStudy.scala)
* [Lazy Study](src/main/scala/values/LazyStudy.scala)

#### Session 3: Inheritance

* [Algebraic Data Type Study](src/main/scala/inheritance/AlgebraicDataTypeStudy.scala)
* [Lambda Case Study](src/main/scala/inheritance/LambdaCaseStudy.scala)
* [Generic Study](src/main/scala/inheritance/GenericStudy.scala)
* [Instance Study](src/main/scala/inheritance/InstanceStudy.scala)
* [Given Study](src/main/scala/inheritance/GivenStudy.scala)
* [Implicit Study](src/main/scala/inheritance/ImplicitStudy.scala)

#### Session 4: Dependencies

* [Repository Study](src/main/scala/dependencies/RepositoryStudy.scala)
* [Comprehension Study](src/main/scala/dependencies/ComprehensionStudy.scala)

#### Session 5: Functional Design Patterns

* [Functional Design Patterns (Study)](src/test/scala/Patterns.scala)

#### Session 6: Tagless Final

* [Tagless Final Study](src/main/scala/tagless/Main.scala)

#### Session 7: Event Sourcing

* [Events Study](src/main/scala/events/Main.scala)

### Literature

* [Gabriel Volpe: Example App](https://github.com/gvolpe)
* [DevInsideYou: Tagless Final](https://www.youtube.com/playlist?list=PLJGDHERh23x-3_T3Dua6Fwp4KlG0J25DI)
* [Cats Tagless](https://github.com/typelevel/cats-tagless)
* [Tagless Final in Scala: A Practical example](https://jproyo.github.io/posts/2019-02-07-practical-tagless-final-in-scala.html)
* [This Is Your App on Scala 3 by John A. De Goes](https://www.youtube.com/watch?v=NY2ZkcYZj54)
* [Scala with Cats](https://www.scalawithcats.com/dist/scala-with-cats.html)

### Architecture (Ports and Adapters)

* `adapters`
  * `ports`
    * `core`
      * `Domain.scala`
    * `Language.scala`
    * `Program.scala`
  * `Interpretation.scala`
  * `Presentation.scala`

```
A1: Primary Adapters      "Presentation"    (EndPoints)  IO
⇩  ⬂
     P1: Primary Ports    "Program"         (UseCases)   F[_]
⇩    ⇩  ⬂
          C: Core Logic   "Domain"          (Models)
⇩    ⇩  ⬀
     P2: Secondary Ports  "Language"        (DSLs)       F[_]
⇩  ⬀
A2: Secondary Adapters    "Interpretation"  (Effects)    IO
```

* `⇩` / `⬂` / `⬀`: "usage"

* [DDD-Architekturen im Vergleich](https://www.maibornwolff.de/blog/ddd-architekturen-im-vergleich)
